//! Implementation of BitTorrent messages as defined in the specification.
//!
//! See https://wiki.theory.org/BitTorrentSpecification#Messages.
use std::fmt::{Debug, Formatter};
use std::io::{Read, Write};
use crate::{alloc_bytes, bytes_to_string_short, Message};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};

#[derive(Debug)]
pub struct BtRequest {
    pub index: u32,
    pub begin: u32,
    pub length: u32,
}

impl BtRequest {
    fn read(input: &mut impl Read) -> std::io::Result<Self> where Self: Sized {
        let index = input.read_u32::<BigEndian>()?;
        let begin = input.read_u32::<BigEndian>()?;
        let length = input.read_u32::<BigEndian>()?;
        Ok(Self { index, begin, length, })
    }

    fn write(&self, output: &mut impl Write) -> std::io::Result<()> {
        output.write_u32::<BigEndian>(self.index)?;
        output.write_u32::<BigEndian>(self.begin)?;
        output.write_u32::<BigEndian>(self.length)
    }
}

pub struct BtPiece {
    pub index: u32,
    pub begin: u32,
    pub block: Vec<u8>,
}

impl BtPiece {
    fn read(input: &mut impl Read, msg_size: usize) -> std::io::Result<Self> where Self: Sized {
        let index = input.read_u32::<BigEndian>()?;
        let begin = input.read_u32::<BigEndian>()?;
        let mut block = alloc_bytes(msg_size - 9);
        input.read_exact(&mut block)?;
        Ok(Self { index, begin, block, })
    }

    fn write(&self, output: &mut impl Write) -> std::io::Result<()> {
        output.write_u32::<BigEndian>(self.index)?;
        output.write_u32::<BigEndian>(self.begin)?;
        output.write_all(&self.block)
    }
}

impl Debug for BtPiece {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        use std::fmt::Write;
        write!(f, "BtPiece {{ index: {}, begin: {}, block: ",
               self.index,
               self.begin)?;
        bytes_to_string_short(&self.block, f)?;
        f.write_char('}')
    }
}

// constants to unify message ID values
//TODO: add some sort of support for this into [strum](https://docs.rs/strum)
const CHOKE: u8 = 0;
const UNCHOKE: u8 = 1;
const INTERESTED: u8 = 2;
const NOT_INTERESTED: u8 = 3;
const HAVE: u8 = 4;
const BITFIELD: u8 = 5;
const REQUEST: u8 = 6;
const PIECE: u8 = 7;
const CANCEL: u8 = 8;
const PORT: u8 = 9;

#[derive(Debug)]
#[repr(u8)]
pub enum BtMessage {
    Choke = CHOKE,
    Unchoke = UNCHOKE,
    Interested = INTERESTED,
    NotInterested = NOT_INTERESTED,
    Have(u32) = HAVE,
    BitField (Vec<u8>) = BITFIELD,
    Request(BtRequest) = REQUEST,
    Piece(BtPiece) = PIECE,
    Cancel(BtRequest) = CANCEL,
    Port(u16) = PORT,
    /// everything else
    Unknown{
        message_id: u8,
        bytes: Vec<u8>,
    },
    /// this type actually has no ID in wire representation, so needs to be handled separately
    KeepAlive,
}

impl Message for BtMessage {
    fn read(input: &mut impl Read) -> std::io::Result<Self> where Self: Sized {
        log::trace!("BtMessage::read: waiting for message...");
        let sz = input.read_u32::<BigEndian>()?;
        if sz == 0 {
            log::debug!("Received keep-alive");
            return Ok(BtMessage::KeepAlive);
        }
        log::trace!("BtMessage::read: sz={sz}");
        let message_id = input.read_u8()?;
        log::trace!("BtMessage::read: message_id={message_id}");
        let message = match message_id {
            CHOKE => BtMessage::Choke,
            UNCHOKE => BtMessage::Unchoke,
            INTERESTED => BtMessage::Interested,
            NOT_INTERESTED => BtMessage::NotInterested,
            HAVE => {
                let piece_index = input.read_u32::<BigEndian>()?;
                BtMessage::Have(piece_index)
            }
            BITFIELD => {
                let mut bits = alloc_bytes((sz - 1) as usize);
                input.read_exact(&mut bits)?;
                BtMessage::BitField(bits)
            }
            REQUEST => BtMessage::Request(BtRequest::read(input)?),
            PIECE => BtMessage::Piece(BtPiece::read(input, sz as usize)?),
            CANCEL => BtMessage::Cancel(BtRequest::read(input)?),
            PORT => BtMessage::Port(input.read_u16::<BigEndian>()?),
            _ => {
                log::error!("Invalid message id: {message_id}");
                let mut bytes = alloc_bytes((sz - 1) as usize);
                input.read_exact(&mut bytes)?;
                BtMessage::Unknown { message_id, bytes }
            },
        };
        log::debug!("Received message: {message:02x?} [hex]");
        Ok(message)
    }

    fn write(&self, output: &mut impl Write) -> std::io::Result<()> {
        log::debug!("Sending message: {self:?}");
        let mut bytes = Vec::new();
        let message_id = match self {
            BtMessage::KeepAlive => {
                output.write_u32::<BigEndian>(0)?;
                return Ok(())
            }
            BtMessage::Choke => CHOKE,
            BtMessage::Unchoke => UNCHOKE,
            BtMessage::Interested => INTERESTED,
            BtMessage::NotInterested => NOT_INTERESTED,
            BtMessage::Have(piece_index) => {
                bytes.write_u32::<BigEndian>(*piece_index)?;
                HAVE
            }
            BtMessage::BitField(bits) => {
                bytes.extend(bits);
                BITFIELD
            }
            BtMessage::Request(req) => {
                req.write(&mut bytes)?;
                REQUEST
            }
            BtMessage::Piece(piece) => {
                piece.write(&mut bytes)?;
                PIECE
            }
            BtMessage::Cancel(req) => {
                req.write(&mut bytes)?;
                CANCEL
            }
            BtMessage::Port(port) => {
                output.write_u16::<BigEndian>(*port)?;
                PORT
            }
            BtMessage::Unknown { message_id, bytes: unknown_bytes } => {
                bytes.extend(unknown_bytes);
                *message_id
            }
        };
        output.write_u32::<BigEndian>((bytes.len() + 1) as u32)?;
        output.write_u8(message_id)?;
        output.write_all(&bytes)
    }
}
