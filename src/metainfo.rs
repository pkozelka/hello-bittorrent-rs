#![allow(dead_code)]

//! Definition of the Metainfo File structure. Also known as "the torrent file".
//!
//! This module is mostly copied from [parse_torrent](https://github.com/CraigglesO/parse-torrent/blob/master/src/torrent.rs).
//!
//! Some pieces come from [cratetorrent].
//!
use bencode::Bencode;
use serde_bencode::de;
use serde_derive::Deserialize;
use serde_derive::Serialize;

use sha1::Digest;
use sha1::Sha1;
use crate::error::BitTorrentError;

#[derive(Debug, Serialize, Deserialize)]
pub struct File {
    name: Option<String>,
    path: String,
    length: u64,
    offset: Option<u64>,
    md5sum: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Info {
    pub name: String,
    #[serde(with = "serde_bytes")]
    pub pieces: Vec<u8>,
    /// this should be the piece length (typically 2^N, most typically 16384 - but the number present here is different. TODO: find out why
    #[serde(rename = "piece length")]
    pub piece_length: u64,
    pub length: Option<u64>,
    pub files: Option<Vec<File>>,
    pub private: Option<u8>,
}

#[derive(/*RustcEncodable, */Serialize)]
pub struct InfoBuf {
    length: u64,
    name: String,
    pieces: Vec<u8>,
    #[serde(rename = "piece length")]
    piece_length: u64,
    private: u8,
}

#[derive(Debug, Deserialize)]
pub struct Torrent {
    pub info: Info,
    #[serde(default)]
    #[serde(rename = "infoBuffer")]
    info_buffer: Vec<u8>,
    #[serde(default)]
    #[serde(rename = "infoHash")]
    info_hash: String,
    #[serde(default)]
    #[serde(rename = "infoHashBuffer")]
    info_hash_buffer: Vec<u8>,
    #[serde(default)]
    name: String,
    #[serde(default)]
    announce: String,
    #[serde(default)]
    #[serde(rename = "announce-list")]
    announce_list: Vec<Vec<String>>,
    #[serde(default)]
    #[serde(rename = "creation date")]
    creation_date: u64,
    #[serde(default)]
    comment: String,
    #[serde(default)]
    #[serde(rename = "created by")]
    created_by: String,
    #[serde(default)]
    #[serde(rename = "urlList")]
    url_list: String,
    #[serde(default)]
    private: bool,
    #[serde(default)]
    length: u64,
    #[serde(default)]
    pieces: Vec<Vec<u8>>,
    #[serde(default)]
    #[serde(rename = "lastPieceLength")]
    last_piece_length: u64,
    #[serde(default)]
    #[serde(rename = "piece length")]
    piece_length: u64,
    #[serde(default)]
    files: Vec<File>,
}

/// A SHA-1 hash digest, 20 bytes long.
pub type Sha1Hash = [u8; 20];

impl Torrent {
    pub fn from_buffer(buffer: &[u8]) -> crate::error::Result<(Torrent, Sha1Hash)> {
        let bdata = bencode::from_buffer(buffer)
            .map_err(|e| crate::error::BitTorrentError::Unknown(e.msg))
            ?;
        let mut info_hash = None;
        match bdata {
            Bencode::Dict(dict_info) => {
                for (key, value) in dict_info {
                    if key.to_string() == r#"s"info""# {
                        let info = value.to_bytes()?;
                        let digest = Sha1::digest(&info);
                        let mut info_hash_x = [0; 20];
                        info_hash_x.copy_from_slice(&digest);
                        info_hash = Some(info_hash_x);
                    }
                }
            }
            _ => return Err(BitTorrentError::BencodeExpectedDictionary)
        }
        let torrent = de::from_bytes::<Torrent>(&buffer)?;
        let info_hash = info_hash.expect("Missing info key!");
        log::trace!("Torrent '{}': Computed info_hash: {info_hash:02X?}", &torrent.info.name);
        Ok((torrent, info_hash))
    }

    pub fn from_file(file: &str) -> crate::error::Result<(Torrent, Sha1Hash)> {
        let buffer = std::fs::read(file)?;
        Ok(Self::from_buffer(&buffer)?)
    }
}

#[cfg(test)]
mod tests {
    use crate::error::Result;

    use crate::metainfo::Sha1Hash;

    #[test]
    fn test_bencode() -> anyhow::Result<()> {
        let s = "Hello world!";
        let encoded = bencode::encode(s)?;
        let encoded = String::from_utf8(encoded)?;
        println!("{encoded}");
        Ok(())
    }

    #[test]
    fn test_parse_torrent() -> Result<()> {
        const INFO_HASH_V1: Sha1Hash = [0x0f, 0x56, 0xf9, 0x74, 0xcc, 0x20, 0x08, 0x13, 0x21, 0x93, 0x36, 0xd5, 0x49, 0x75, 0xdf, 0xc7, 0xdf, 0x0e, 0xdd, 0xc3];
        let file = "data/jdk-12.0.2_linux-x64_bin.tar.gz.torrent";
        let (torrent, info_hash) = super::Torrent::from_file(&file)?;
        println!("{torrent:?}");
        println!("info_hash={info_hash:02X?}");
        assert_eq!(INFO_HASH_V1, info_hash, "Mismatch in `info_hash`");
        Ok(())
    }
}
