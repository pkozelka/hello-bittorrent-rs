use std::convert::Infallible;
use std::time::UNIX_EPOCH;
use std::fmt::{Display, Formatter};
use std::ops::Deref;
use std::str::FromStr;

/// Identification of one side of a P2P connection.
///
/// See detailed specification: https://wiki.theory.org/BitTorrentSpecification#peer_id
pub struct PeerId(pub [u8;20]);

impl Deref for PeerId {
    type Target = [u8;20];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl PeerId {
    /// Somewhat hard-coded way of generating the identification in "usual" format,
    /// based on prefix `hb` (stands for Hello Bittorrent), fake version `0001` and current timestamp.
    pub(crate) fn generate() -> Self {
        let timestamp = std::time::SystemTime::now()
            .duration_since(UNIX_EPOCH).unwrap() // never fails
            .as_secs();
        let id = format!("-hb0001-{timestamp:0>12}");
        Self::from_str(&id).unwrap() // infallible
    }
}

impl FromStr for PeerId {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut id: [u8;20] = [0;20];
        id.copy_from_slice(s.as_bytes());
        Ok(Self(id))
    }
}

impl Display for PeerId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        String::from_utf8_lossy(&self.0).fmt(f)
    }
}
