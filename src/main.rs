extern crate core;

use std::net::TcpStream;

use clap::{Parser, Subcommand};

use hello_bittorrent::{BitTorrentLink, BitTorrentNode};

fn main() -> anyhow::Result<()> {
    let cli = Cli::with_logging(module_path!());
    match cli.command {
        Commands::Handshake { torrent_file } => {
            node_connect(&cli.peer, &torrent_file)?;
            log::info!("Handshake succeeded");
        }
        Commands::Download { torrent_file } => {
            let (tcp_stream, mut node) = node_connect(&cli.peer, &torrent_file)?;
            node.selfish_receive(tcp_stream)?;
        }
    }
    Ok(())
}

/// Connect to another peer
fn node_connect(peer: &str, torrent_file: &str) -> anyhow::Result<(TcpStream, BitTorrentNode)> {
    let mut tcp_stream = TcpStream::connect(&peer)?;
    let link = BitTorrentLink::handshake(&torrent_file, &mut tcp_stream)?;
    let node = BitTorrentNode::new(link)?;
    Ok((tcp_stream, node))
}

#[derive(Subcommand)]
enum Commands {
    /// Initiate handshake and stop once it succeeds
    Handshake {
        torrent_file: String,
    },
    /// Full selfish download (just pretended!)
    Download {
        torrent_file: String,
    },
}

#[derive(Parser)]
struct Cli {
    /// Logging in verbose mode (-v = DEBUG, -vv = TRACE)
    #[clap(short, long, action = clap::ArgAction::Count)]
    verbose: u8,
    /// Logging in silent mode (-s = WARN, -ss = ERROR, -sss = OFF)
    #[clap(short, long, action = clap::ArgAction::Count)]
    silent: u8,
    /// Address of the listening peer
    peer: String,
    #[command(subcommand)]
    command: Commands,
}

impl Cli {
    pub fn with_logging(module: &str) -> Self {
        let cli = Self::parse();
        let level = 2 as i16 + cli.verbose as i16 - cli.silent as i16;
        stderrlog::new()
            .modules(vec![module, module_path!()])
            .quiet(level < 0)
            .verbosity(level as usize)
            .timestamp(stderrlog::Timestamp::Millisecond)
            .init()
            .unwrap();
        log::debug!("Logging started");
        cli
    }
}
