//! Library API for working with bittorrent messages.

extern crate core;

use std::fmt::{Formatter, UpperHex};
use std::io::{Read, Write};

pub use handshake::BitTorrentLink;
pub use peerid::PeerId;
pub use node::BitTorrentNode;
pub use error::{BitTorrentError,Result};

mod handshake;
mod peerid;
mod metainfo;
mod message;
mod node;
mod error;

/// Quickly allocate zeroed memory.
#[inline]
fn alloc_bytes(count: usize) -> Vec<u8> {
    (0..count).map(|_| 0_u8).collect()
}

/// A unification of messages, both for handshake and protocol events.
trait Message {
    /// Deserialize message from received bytes
    fn read(input: &mut impl Read) -> std::io::Result<Self>
        where Self: Sized;

    /// Serialize message into bytes
    fn write(&self, output: &mut impl Write) -> std::io::Result<()>;
}

fn bytes_to_hex(bytes: &[u8], f: &mut Formatter<'_>) -> std::fmt::Result {
    if bytes.len() > 0 {
        UpperHex::fmt(&bytes[0], f)?;
        for b in &bytes[1..] {
            use std::fmt::Write;
            Formatter::write_char(f, ',')?;
            UpperHex::fmt(b, f)?;
        }
    }
    Ok(())
}

fn bytes_to_string_short(bytes: &[u8], f: &mut Formatter<'_>) -> std::fmt::Result {
    use std::fmt::Write;
    write!(f, "<sz={}:", bytes.len())?;
    if bytes.len() < 32 {
        bytes_to_hex(bytes, f)?;
    } else {
        f.write_char('[')?;
        bytes_to_hex(&bytes[0..5], f)?;
        Formatter::write_str(f, "...")?;
        bytes_to_hex(&bytes[bytes.len()-5..], f)?;
        f.write_char(']')?;
    }
    f.write_char('>')
}
