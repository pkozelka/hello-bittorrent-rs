//! Implementation(s) of the protocol.
//!
//! Currently, no advanced strategies are implemented.

use std::io::Write;
use std::net::TcpStream;
use crate::{BitTorrentLink, Message};
use crate::message::{BtMessage, BtRequest};

/// Piece size to use. We work with the recommended value.
const BLOCK_SIZE: u32 = 16384; // 2^14

/// Node contains link info and download state
pub struct BitTorrentNode {
    link: BitTorrentLink,
    /// this client is choking the peer
    am_choking: bool,
    /// this client is interested in the peer
    am_interested: bool,
    /// peer is choking this client
    peer_choking: bool,
    /// peer is interested in this client
    peer_interested: bool,

    /// for simple download:
    received_pieces: u32,
}

impl BitTorrentNode {
    /// Create new instance of BitTorrent node, ready to exchange protocol messages.
    pub fn new(link: BitTorrentLink) -> crate::error::Result<Self> {
        Ok(Self {
            link ,
            am_choking: true,
            am_interested: false,
            peer_choking: true,
            peer_interested: false,
            received_pieces: 0,
        })
    }

    /// Selfish download implementation.
    ///
    /// It just pretends download - just asks blocks one by one srquentially, only to examine the protocol.
    /// Real implementation needs to do a lot of other stuff - store pieces, keep track of missing
    /// ones, request them in random (or somehow sophisticated) order etc.
    pub fn selfish_receive(&mut self, mut stream: TcpStream) -> crate::error::Result<()> {
        // BtMessage::BitField(Vec::new()).write(&mut stream)?; // we don't have anything yet so we can skip
        BtMessage::Interested.write(&mut stream)?;

        let mut should_continue = true;
        while should_continue {
            if ! self.peer_choking {
                // ask for next block
                let req_msg = BtMessage::Request(BtRequest{
                    index: self.received_pieces,
                    begin: 0,
                    length: BLOCK_SIZE,
                });
                req_msg.write(&mut stream)?;
            }
            //
            let message = BtMessage::read(&mut stream)?;
            should_continue = self.process_message(message, &mut stream)?;
        }
        Ok(self.graceful_stop(&mut stream))
    }

    /// Very incomplete message handler
    pub fn process_message(&mut self, message: BtMessage, writer: &mut impl Write) -> crate::error::Result<bool> {
        log::debug!("Processing message: {message:02X?}");
        match &message {
            BtMessage::KeepAlive => {
                message.write(writer)?;
                log::debug!("file: {}, am_choking: {}, am_interested: {}, peer_choking: {}, peer_interested: {}",
                    self.link.torrent.info.name,
                    self.am_choking,
                    self.am_interested,
                    self.peer_choking,
                    self.peer_interested,
                )
            },
            BtMessage::BitField(his_pieces) => {
                //TODO store his pieces
                log::info!("his_pieces.len()={}", his_pieces.len());
            }
            BtMessage:: Choke => {
                if self.peer_choking {
                    log::debug!("WARNING: Peer is choking me, again?");
                }
                self.peer_choking = true
            },
            BtMessage::Unchoke => {
                if !self.peer_choking {
                    log::debug!("WARNING: Peer is unchoking me, again?");
                }
                self.peer_choking = false;
            },
            BtMessage::Interested => self.peer_interested = {
                if self.peer_interested {
                    log::debug!("WARNING: Peer is interested, again?");
                }
                true
            },
            BtMessage::NotInterested => self.peer_interested = {
                if !self.peer_interested {
                    log::debug!("WARNING: Peer is un-interested, again?");
                }
                false
            },

            BtMessage::Piece(piece) => {
                //TODO instead, store it
                self.received_pieces = piece.index + 1; // this just preteds
                //
                BtMessage::Have(piece.index).write(writer)?;
            }
            BtMessage::Have(_index) => {
                log::error!("NOT IMPLEMENTED YET: {message:?}");
            }
            BtMessage::Request(_request) => {
                log::error!("NOT IMPLEMENTED YET: {message:?}");
            }
            BtMessage::Cancel(_request) => {
                log::error!("NOT IMPLEMENTED YET: {message:?}");
            }
            BtMessage::Port(_port) => {
                log::error!("NOT IMPLEMENTED YET: {message:?}");
            }
            BtMessage::Unknown { message_id, bytes } => {
                log::error!("UNKNOWN MESSAGE: ID={message_id}, content: {} bytes", bytes.len());
                return Ok(false);
            }
        }
        Ok(true)
    }

    fn graceful_stop(&mut self, _writer: &mut impl Write) {
        //TODO depending on state, do some goodbye stuff here
    }
}
