/// BitTorrent Handshake implementation.
///
/// See https://wiki.theory.org/BitTorrentSpecification#Handshake for details.

use std::io::{Read, Write};
use byteorder::{BigEndian, ReadBytesExt, WriteBytesExt};
use crate::{alloc_bytes, Message, PeerId};
use crate::error::BitTorrentError;
use crate::metainfo::{Sha1Hash, Torrent};

/// The string used to identify the protocol.
pub const PROTOCOL_STRING: &str = "BitTorrent protocol";

/// BitTorrent Handshake.
///
/// The handshake consists of two messages, here names as [Handshake1] and [Handshake2].
///
/// [Handshake1] is always mandatory, and serves for exchanging information about protocol and the file to be transferred.
///
/// [Handshake2] is omitted only for the tracker's NAT-checking feature; for normal peers, it's mandatory as well.
///
/// Note: the word `Link` in name is just a second best alternative for `Connection` which would be misleading.
pub struct BitTorrentLink {
    pub my_id: PeerId,
    pub info_hash: Sha1Hash,
    pub his_id: PeerId,
    pub torrent: Torrent,
}

impl BitTorrentLink {

    /// Synchronous implementation of the handshake.
    pub fn handshake<S>(torrent_file: &str, stream: &mut S) -> crate::error::Result<Self>
    where S: Read + Write,
    {
        let (torrent, info_hash) = Torrent::from_file(torrent_file)?;

        log::info!("Torrent: name='{}', size={:?} bytes and has {} pieces [private:{:?}]",
                 torrent.info.name,
                 torrent.info.length,
                 torrent.info.pieces.len(),
                 torrent.info.private,
        );
        log::trace!("... piece length: {:02x}", torrent.info.piece_length);
        log::debug!("Handshake1: Sending info_hash: {:02X?}", info_hash);
        let hs1 = Handshake1::new(info_hash);
        hs1.write(stream)?;
        log::trace!("Handshake1: Waiting for response...");
        let hs1 = Handshake1::read(stream)?;
        if hs1.pstr != PROTOCOL_STRING {
            return Err(BitTorrentError::UnsupportedProtocol(hs1.pstr));
        }
        if hs1.reserved != 0 {
            log::warn!("non-zero bits in reserved area: {:02x?}", &hs1.reserved.to_le_bytes());
        }
        if hs1.info_hash != info_hash {
            return Err(BitTorrentError::InvalidInfoHash(hs1.info_hash));
        }

        let id = PeerId::generate();
        log::debug!("Handshake2: Sending my id: {}", id);
        let my_hs2 = Handshake2 { peer_id: id };
        my_hs2.write(stream)?;
        log::trace!("Handshake2: Waiting for response...");
        let his_hs2 = Handshake2::read(stream)?;
        log::debug!("Handshake2: Received his id: '{}'", his_hs2.peer_id);
        Ok(Self {
            my_id: my_hs2.peer_id,
            info_hash,
            his_id: his_hs2.peer_id,
            torrent
        })
    }
}

/// First part of BitTorrent handshake.
/// If accepted, the [Handshake2] follows.
pub struct Handshake1 {
    // protocol string, normall "BitTorrent protocol"
    pub pstr: String,
    pub reserved: u64,
    pub info_hash: Sha1Hash,
}

impl Handshake1 {
    pub fn new(info_hash: Sha1Hash) -> Self {
        Self {
            pstr: PROTOCOL_STRING.to_string(),
            reserved: 0,
            info_hash,
        }
    }
}

impl Message for Handshake1 {
    fn read(input: &mut impl Read) -> std::io::Result<Self> {
        // protocol string
        let mut pstrlen = [0_u8;1];
        input.read_exact(&mut pstrlen)?;
        let mut pstr = alloc_bytes(pstrlen[0] as usize);
        input.read_exact(&mut pstr)?;
        let pstr = String::from_utf8_lossy(&pstr).to_string();

        let mut result = Self {
            pstr,
            reserved: 0,
            info_hash: Default::default(),
        };
        // reserved
        result.reserved = input.read_u64::<BigEndian>()?;
        // info_hash
        input.read_exact(&mut result.info_hash)?;
        Ok(result)
    }

    fn write(&self, output: &mut impl Write) -> std::io::Result<()> {
        // protocol string
        output.write_u8(self.pstr.len() as u8)?;
        output.write_all(self.pstr.as_bytes())?;
        //
        output.write_u64::<BigEndian>(self.reserved)?;
        output.write_all(&self.info_hash)?;
        Ok(())
    }
}

/// Second part of BitTorrent handshake.
///
/// See also [Handshake1]
pub struct Handshake2 {
    /// ID of the sending party.
    pub peer_id: PeerId,
}

impl Message for Handshake2 {
    fn read(input: &mut impl Read) -> std::io::Result<Self> where Self: Sized {
        let mut peer_id: [u8;20] = Default::default();
        input.read_exact(&mut peer_id)?;
        Ok(Self { peer_id: PeerId(peer_id) })
    }

    fn write(&self, output: &mut impl Write) -> std::io::Result<()> {
        output.write_all(&*self.peer_id)
    }
}
