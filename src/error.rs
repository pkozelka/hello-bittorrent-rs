use thiserror::Error as ThisError;
use crate::metainfo::Sha1Hash;

pub type Result<T> = std::result::Result<T, BitTorrentError>;

#[derive(ThisError, Debug)]
pub enum BitTorrentError {
    #[error("Unsupported protocol: {0}")]
    UnsupportedProtocol(String),
    #[error("Invalid info hash: {0:02X?}")]
    InvalidInfoHash(Sha1Hash),
    #[error("bencode: Expected dictionary")]
    BencodeExpectedDictionary,
    #[error(transparent)]
    IoError(#[from] std::io::Error),
    #[error(transparent)]
    SerdeBencode(#[from] serde_bencode::Error),
    #[error("unknown error: {0:?}")]
    Unknown(String),
}
