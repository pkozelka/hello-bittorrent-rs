# BitTorrent experimental implementation

This project contains partial and extremely simplistic implementation of the BitTorrent protocol.

The goal is to learn a bit about principles used in bittorrent, and about P2P in general.

## How to test handshake

The testing is done by running this program against locally started bittorrent client.
I used `qBittorrent` for ubuntu, and it will be used in the following description, but most bittorrent clients should work just fine.

- have some file that will be "transferred" - let's call it `somefile.bin`
- run your bittorrent client, find out which TCP port is it running on
  - in `qB`, you will see it in the tab `Execution Log` - normally, it is `27731`
- create `.torrent` file for it
  - in `qB`, it's in the menu: `Tools | Torrent Creator`
  - select: Torrent format = `V1`
  - select: Private torrent (Won't distribute on DHT network)
  - select: Start seeding immediately
  - select: Ignore share ratio limits for this torrent
- and save it somewhere; let's refer to this file as `/some/path/somefile.bin.torrent`
- now, compile and run the application:
  ```
  cargo run -- 127.0.0.1:27731 handshake /some/path/somefile.bin.torrent
  ```
- messages exchanged during the communication will be displayed on the console

## Limitations

There are many, let's just list most obvious ones:

- only protocol V1 over TCP is supported
- no extensions are implemented
- only single file can be in the torrent

Plus, at the time of writing, the implementation:
- does not implement tracker (HTTP server)
- only _pretends_ downloading the file, doesn't store anything to disk
- performs only "selfish" download, ie. no contributions back to the network (that's why we really need "Ignore share ratio..." when creating torrent)

## Next steps

- implement listening side
- switch to async with tokio
- add real saving
- add upload
- ... etc

## Resources

- [The specification](http://bittorrent.org/beps/bep_0003.html)
  - slightly more readable alternative: https://wiki.theory.org/BitTorrentSpecification
- Some articles and inspiration
  - [Writing a Bittorrent engine in Rust](https://mandreyel.github.io/posts/rust-bittorrent-engine/)
  - [Message sequence in a typical download](https://www.researchgate.net/figure/Message-sequence-in-a-typical-download_fig2_223808116)
