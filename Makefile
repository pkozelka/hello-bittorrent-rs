all: build

clean:
	cargo clean

build:
	cargo build --lib --bins --examples --tests

test:
	cargo test

run-handshake:
	cargo run -- -vv 127.0.0.1:27731 handshake data/prusa3d_linux_2_5_0.zip.torrent

run-download:
	cargo run -- -vv 127.0.0.1:27731 download data/prusa3d_linux_2_5_0.zip.torrent

qbittorrent:
	qbittorrent --version || sudo apt install qbittorrent -y
	nohup qbittorrent&
